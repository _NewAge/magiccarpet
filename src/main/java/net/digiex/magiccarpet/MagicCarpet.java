package net.digiex.magiccarpet;

import static org.bukkit.Material.BRICK;
import static org.bukkit.Material.CLAY;
import static org.bukkit.Material.COBBLESTONE;
//import static org.bukkit.Material.DOUBLE_STEP;
import static org.bukkit.Material.END_STONE;
import static org.bukkit.Material.GLASS;
import static org.bukkit.Material.GLOWSTONE;
import static org.bukkit.Material.HAY_BLOCK;
//import static org.bukkit.Material.LOG;
import static org.bukkit.Material.OAK_LOG;
import static org.bukkit.Material.SPRUCE_LOG;
import static org.bukkit.Material.BIRCH_LOG;
import static org.bukkit.Material.ACACIA_LOG;
import static org.bukkit.Material.DARK_OAK_LOG;
import static org.bukkit.Material.JUNGLE_LOG;
import static org.bukkit.Material.MOSSY_COBBLESTONE;
import static org.bukkit.Material.NETHERRACK;
import static org.bukkit.Material.NETHER_BRICK;
import static org.bukkit.Material.OBSIDIAN;
import static org.bukkit.Material.PRISMARINE;
import static org.bukkit.Material.SANDSTONE;
import static org.bukkit.Material.SEA_LANTERN;
//import static org.bukkit.Material.HARD_CLAY;
import static org.bukkit.Material.TERRACOTTA;
import static org.bukkit.Material.WHITE_TERRACOTTA;
import static org.bukkit.Material.ORANGE_TERRACOTTA;
import static org.bukkit.Material.MAGENTA_TERRACOTTA;
import static org.bukkit.Material.LIGHT_BLUE_TERRACOTTA;
import static org.bukkit.Material.YELLOW_TERRACOTTA;
import static org.bukkit.Material.LIME_TERRACOTTA;
import static org.bukkit.Material.PINK_TERRACOTTA;
import static org.bukkit.Material.LIGHT_GRAY_TERRACOTTA;
import static org.bukkit.Material.GRAY_TERRACOTTA;
import static org.bukkit.Material.CYAN_TERRACOTTA;
import static org.bukkit.Material.BLUE_TERRACOTTA;
import static org.bukkit.Material.BROWN_TERRACOTTA;
import static org.bukkit.Material.PURPLE_TERRACOTTA;
import static org.bukkit.Material.GREEN_TERRACOTTA;
import static org.bukkit.Material.RED_TERRACOTTA;
import static org.bukkit.Material.BLACK_TERRACOTTA;
//import static org.bukkit.Material.STAINED_GLASS;
import static org.bukkit.Material.WHITE_STAINED_GLASS;
import static org.bukkit.Material.ORANGE_STAINED_GLASS;
import static org.bukkit.Material.MAGENTA_STAINED_GLASS;
import static org.bukkit.Material.LIGHT_BLUE_STAINED_GLASS;
import static org.bukkit.Material.YELLOW_STAINED_GLASS;
import static org.bukkit.Material.LIME_STAINED_GLASS;
import static org.bukkit.Material.PINK_STAINED_GLASS;
import static org.bukkit.Material.LIGHT_GRAY_STAINED_GLASS;
import static org.bukkit.Material.GRAY_STAINED_GLASS;
import static org.bukkit.Material.CYAN_STAINED_GLASS;
import static org.bukkit.Material.BLUE_STAINED_GLASS;
import static org.bukkit.Material.BROWN_STAINED_GLASS;
import static org.bukkit.Material.GREEN_STAINED_GLASS;
import static org.bukkit.Material.RED_STAINED_GLASS;
import static org.bukkit.Material.PURPLE_STAINED_GLASS;
import static org.bukkit.Material.BLACK_STAINED_GLASS;
import static org.bukkit.Material.STONE;
import static org.bukkit.Material.OAK_PLANKS;
import static org.bukkit.Material.SPRUCE_PLANKS;
import static org.bukkit.Material.BIRCH_PLANKS;
import static org.bukkit.Material.ACACIA_PLANKS;
import static org.bukkit.Material.DARK_OAK_PLANKS;
import static org.bukkit.Material.JUNGLE_PLANKS;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.EnumSet;
import java.util.logging.Logger;

import org.bukkit.Material;
import org.bukkit.event.Listener;
import org.bukkit.plugin.java.JavaPlugin;

import net.digiex.magiccarpet.plugins.Plugins;

/*
 * Magic Carpet 2.4 Copyright (C) 2012-2014 Android, Celtic Minstrel, xzKinGzxBuRnzx
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 2 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */
public class MagicCarpet extends JavaPlugin {

    private static EnumSet<Material> acceptableCarpet = EnumSet.of(STONE, COBBLESTONE, OAK_PLANKS, SPRUCE_PLANKS, BIRCH_PLANKS, ACACIA_PLANKS, DARK_OAK_PLANKS, JUNGLE_PLANKS, OAK_LOG, DARK_OAK_LOG, SPRUCE_LOG, BIRCH_LOG, JUNGLE_LOG, ACACIA_LOG, GLASS,  SANDSTONE, /*DOUBLE_STEP,*/ BRICK, MOSSY_COBBLESTONE, OBSIDIAN, CLAY, NETHERRACK, NETHER_BRICK, END_STONE, TERRACOTTA, WHITE_TERRACOTTA, ORANGE_TERRACOTTA, MAGENTA_TERRACOTTA, LIGHT_BLUE_TERRACOTTA, YELLOW_TERRACOTTA, LIME_TERRACOTTA, PINK_TERRACOTTA, GRAY_TERRACOTTA, LIGHT_GRAY_TERRACOTTA, CYAN_TERRACOTTA, PURPLE_TERRACOTTA, BLUE_TERRACOTTA, BROWN_TERRACOTTA, GREEN_TERRACOTTA, RED_TERRACOTTA, BLACK_TERRACOTTA, HAY_BLOCK, /*STAINED_GLASS,*/ WHITE_STAINED_GLASS, ORANGE_STAINED_GLASS, MAGENTA_STAINED_GLASS, LIGHT_BLUE_STAINED_GLASS, YELLOW_STAINED_GLASS, LIME_STAINED_GLASS, PINK_STAINED_GLASS, LIGHT_GRAY_STAINED_GLASS, GRAY_STAINED_GLASS, CYAN_STAINED_GLASS, PURPLE_STAINED_GLASS, BLUE_STAINED_GLASS, BLUE_STAINED_GLASS, BROWN_STAINED_GLASS, GREEN_STAINED_GLASS, RED_STAINED_GLASS, BLACK_STAINED_GLASS, PRISMARINE);
    private static EnumSet<Material> acceptableLight = EnumSet.of(GLOWSTONE, SEA_LANTERN);

    private static Logger log;
    private static Storage carpets;
    private static Magic magic = new Magic();

    private void registerCommands() {
        getCommand("magiccarpet").setExecutor(new net.digiex.magiccarpet.commands.Carpet());
        getCommand("magiclight").setExecutor(new net.digiex.magiccarpet.commands.Light());
        getCommand("magiccarpetbuy").setExecutor(new net.digiex.magiccarpet.commands.Buy());
        getCommand("magicreload").setExecutor(new net.digiex.magiccarpet.commands.Reload());
        getCommand("carpetswitch").setExecutor(new net.digiex.magiccarpet.commands.Switch());
        getCommand("magictools").setExecutor(new net.digiex.magiccarpet.commands.Tool());
    }

    private void registerEvents(final Listener listener) {
        getServer().getPluginManager().registerEvents(listener, this);
    }

    private File carpetsFile() {
        return new File(getDataFolder(), "carpets.dat");
    }

    @Override
    public void onDisable() {
        if (Config.getSaveCarpets())
            saveCarpets();
        else
            for (final Carpet c : carpets.all()) {
                if (c == null || !c.isVisible())
                    continue;
                c.removeCarpet();
            }
        log.info("is now disabled!");
    }

    @Override
    public void onEnable() {
        log = getLogger();
        if (!getDataFolder().exists())
            getDataFolder().mkdirs();
        new Config(this);
        carpets = new Storage();
        new Plugins(this);
        if (Config.getSaveCarpets())
            loadCarpets();
        new Permissions();
        registerEvents(new Listeners());
        registerCommands();
        log.info("is now enabled!");
    }

    public void saveCarpets() {
        final File carpetDat = carpetsFile();
        log.info("Saving carpets...");
        if (!carpetDat.exists())
            try {
                carpetDat.createNewFile();
            } catch (final Exception e) {
                log.severe("Unable to create carpets.dat");
            }
        try {
            final FileOutputStream file = new FileOutputStream(carpetDat);
            final ObjectOutputStream out = new ObjectOutputStream(file);
            out.writeObject(carpets);
            out.close();
        } catch (final Exception e) {
            log.warning("Error writing to carpets.dat; carpets data has not been saved!");
        }
        carpets.clear();
    }

    public void loadCarpets() {
        final File carpetDat = carpetsFile();
        if (!carpetDat.exists())
            return;
        log.info("Loading carpets...");
        try {
            final FileInputStream file = new FileInputStream(carpetDat);
            final ObjectInputStream in = new ObjectInputStream(file);
            carpets = (Storage) in.readObject();
            in.close();
        } catch (final Exception e) {
            log.warning("Error loading carpets.dat; it may be corrupt and will be overwritten with new data.");
            return;
        }
        carpets.checkCarpets();
    }

    public static Storage getCarpets() {
        return carpets;
    }

    public static Logger log() {
        return log;
    }

    public static Magic getMagic() {
        return magic;
    }

    public static EnumSet<Material> getAcceptableCarpetMaterial() {
        return acceptableCarpet;
    }

    public static EnumSet<Material> getAcceptableLightMaterial() {
        return acceptableLight;
    }
}
